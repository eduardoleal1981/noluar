#!/bin/bash
# Build docker image
docker build -t tserverimage:latest .
# Run docker container
# Executar fora do script, sem o -d de detached
# docker run --name tServer -d -p 8080:8080 tserverimage:latest